package at.spenger.jpa;


import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.spenger.jpa.Application;
import at.spenger.jpa.model.Album;
import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Track;
import at.spenger.jpa.service.AlbumRepository;
import at.spenger.jpa.service.ArtistRepository;
import at.spenger.jpa.service.TrackRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ApplicationTests {

	@Autowired
	private TrackRepository trackRepository;
	@Autowired
	private ArtistRepository artistRepository;
	@Autowired
	private AlbumRepository albumRepository;
	
	@Test
	public void checkBillyPreston()
	{
		List<Artist> result = artistRepository.findByNameLike("Billy Preston");
		assertThat(result.size(),is(1));
		assertThat(result.get(0).getName(), equalTo("Billy Preston"));
		assertThat(result.get(0).getAlbums().size(),is(1));
		assertThat(result.get(0).getAlbums().get(0).getTitle(),equalTo("The Most Exciting Organ Ever"));
	}
	
	@Test
	public void checkAlbum()
	{
		List<Album> result = albumRepository.findByNameLike("The Most Exciting Organ Ever");
		assertThat(result.size(),is(1));
		assertThat(result.get(0).getTitle(), equalTo("The Most Exciting Organ Ever"));
		assertThat(result.get(0).getTracks().size(),is(10));
		for(Track t:result.get(0).getTracks())
			assertThat(t.getName(),not(equalTo("INVALID TRACK")));
	}

}
