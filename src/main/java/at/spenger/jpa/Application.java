package at.spenger.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import at.spenger.jpa.model.Album;
import at.spenger.jpa.model.Artist;
import at.spenger.jpa.model.Mediatype;
import at.spenger.jpa.model.Playlist;
import at.spenger.jpa.model.Track;
import at.spenger.jpa.service.AlbumRepository;
import at.spenger.jpa.service.ArtistRepository;
import at.spenger.jpa.service.MediaTypeRepository;
import at.spenger.jpa.service.PlayListRepository;
import at.spenger.jpa.service.PlayListService;
import at.spenger.jpa.service.TrackRepository;


@Import(Config.class)
public class Application implements CommandLineRunner {

	@Autowired
	private TrackRepository trackRepository;
	@Autowired
	private ArtistRepository artistRepository;
	@Autowired
	private AlbumRepository albumRepository;
	@Autowired
	private MediaTypeRepository mediaTypeRepository;
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	@Transactional
	public void run(String... arg0) throws Exception
	{		
		deleteArtistByName("Billy Preston");
		Artist billy = createArtist("Billy Preston");
		
		Album tmeoe = createAlbum("The Most Exciting Organ Ever",billy);
		
		createTrack("If I Had A Hammer", tmeoe);
		createTrack("Lowdown", tmeoe);
		createTrack("Slippin' and Slidin'", tmeoe);
		createTrack("Drown In My Own Tears", tmeoe);
		createTrack("I Am Coming Through", tmeoe);
		createTrack("The Octopus", tmeoe);
		createTrack("Soul Meeting", tmeoe);
		createTrack("Let Me Know", tmeoe);
		createTrack("Billy's Bag", tmeoe);
		createTrack("The Masquerade Is Over", tmeoe);
		
		createTrack("INVALID TRACK",tmeoe);
		deleteTrackByName("INVALID TRACK");
	}
	
/*	private void testRead() {
		List<Playlist> l = playListService.findAll();
		System.out.println(l.get(0));
	}*/
	
	/**
	 * 
	 * @param name
	 */
	private Artist createArtist(String name)
	{
		Artist a = new Artist();
		a.setName(name);
		
		return artistRepository.save(a);
	}
	
	private void deleteArtistByName(String name)
	{
		List<Artist> artists = artistRepository.findByNameLike(name);
		if(artists.size()==0)
		{
			System.out.println("No artists match the name: "+name);
		}
		else if(artists.size()>1)
			System.out.println("There is more than one artist matching the name pattern: "+name);
		else
		{
			for(Album a : artists.get(0).getAlbums())
				deleteAlbumByName(a.getTitle());
			artistRepository.delete(artists.get(0));
		}
	}
	
	private void addAlbumToArtist(Album album, String artistName)
	{
		List<Artist> artists = artistRepository.findByNameLike(artistName);
		if(artists.size()==0)
		{
			System.out.println("No artists match the name: "+artistName);
		}
		else if(artists.size()>1)
			System.out.println("There is more than one artist matching the name: "+artistName);
		else
		{
			artists.get(0).addAlbum(album);
		}
	}
	
	private Album createAlbum(String name, Artist artist)
	{
		Album a = new Album();
		a.setTitle(name);
		
		a.setArtist(artist);
		return albumRepository.save(a);
	}
		
	private void deleteAlbumByName(String name)
	{
		List<Album> albums = albumRepository.findByNameLike(name);
		if(albums.size()==0)
		{
			System.out.println("No album matches the name: "+name);
		}
		else if(albums.size()>1)
			System.out.println("There is more than one album matching the name: "+name);
		else
		{
			for(Track t: albums.get(0).getTracks())
				deleteTrackByName(t.getName());
			albumRepository.delete(albums.get(0));
		}
	}
	
	public Track createTrack(String name, Album album)
	{
		Track t = new Track();
		t.setName(name);
		t.setAlbum(album);
		t.setMediatype(mediaTypeRepository.findAll().get(0));
		t.setUnitPrice(new BigDecimal(1));
		return trackRepository.save(t);
	}
	
	private void deleteTrackByName(String name)
	{
		List<Track> tracks = trackRepository.findByNameLike(name);
		if(tracks.size()==0)
		{
			System.out.println("No track matches the name: "+name);
		}
		else if(tracks.size()>1)
			System.out.println("There is more than one track matching the name: "+name);
		else
		{
			trackRepository.delete(tracks.get(0));
		}
	}
}
