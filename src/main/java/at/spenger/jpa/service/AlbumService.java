package at.spenger.jpa.service;

import java.util.List;

import at.spenger.jpa.model.Album;

public interface AlbumService {
	List<Album> findByNameLike(String name);
	Album findOne(int id);
	Album save(Album a);
	void delete(int id);
	long length();
}
