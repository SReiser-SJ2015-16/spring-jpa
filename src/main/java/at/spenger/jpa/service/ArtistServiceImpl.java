package at.spenger.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import at.spenger.jpa.model.Artist;


@Component("artistService")
@Transactional	// Spring provides a transaction manager specifically for JPA.
public class ArtistServiceImpl implements ArtistService {
	private final ArtistRepository artistRepository;
	@Autowired
	public ArtistServiceImpl(ArtistRepository artistRepository) {
		this.artistRepository = artistRepository;
	}

	@Override
	@Transactional(readOnly=true)
	public List<Artist> findByNameLike(String name) {
		return artistRepository.findByNameLike(name);
	}
	@Override
	@Transactional(readOnly=true)
	public Artist findOne(int id) {
		return artistRepository.findOne(id);
	}
	@Override
	public Artist save(Artist a) {
		return artistRepository.save(a);
	}
	@Override
	public void delete(int id) {
		artistRepository.delete(id);
	}
	@Override
	@Transactional(readOnly=true)
	public long length() {
		return artistRepository.count();
	}
	

}
