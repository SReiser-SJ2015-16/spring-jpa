package at.spenger.jpa.service;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import at.spenger.jpa.model.Mediatype;
import at.spenger.jpa.model.Track;

public interface MediaTypeRepository extends CrudRepository<Mediatype, Integer>
{
	List<Mediatype> findAll();
}
